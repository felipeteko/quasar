import {RouterView} from "vue-router";

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: {
      auth: true
    },
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'administracion',
        component: RouterView,
        children: [
          {
            path: '',
            redirect: '/administracion/usuarios'
          },
          {
            path: 'usuarios',
            component: RouterView,
            children: [
              {
                path: '',
                component: () => import('pages/administracion/Usuarios.vue')
              },
              {
                path: 'nuevo',
                component: () => import('pages/administracion/NuevoUsuario.vue')
              }
            ]
          }
        ]
      },
    ],
  },
  {
    path: '/users',
    component: () => import('layouts/Users.vue'),
    children: [
      {
        path: '',
        redirect: '/users/login'
      },
      {
        path: 'login',
        component: () => import('pages/users/Login.vue')
      }
    ]
  },
  {
    path: '/:catchAll(.*)*',
    name: '404',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
