import { boot } from 'quasar/wrappers'
import * as localForage from "localforage";

const storage = localForage.createInstance({
  name: process.env.APP_NAME
});

async function getToken(){
  return await storage.getItem('token');
}

async function setToken(token){
  return await storage.setItem('token', token);
}

async function removeToken(){
  return await storage.removeItem('token');
}

export default boot(() => {

});

export {storage, getToken, setToken, removeToken};
