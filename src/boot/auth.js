import { boot } from 'quasar/wrappers'
import {useUserStore} from "src/stores/user";
import {watch} from "vue";

export default boot( async ({ router }) => {
  const userStore = useUserStore();

  await userStore.validate();

  watch(() => userStore.isLoggedIn, (val) => {
    if(val){
      router.replace('/');
    } else {
      router.replace('/users/login');
    }
  });

  //Middleware
  router.beforeEach((to, from, next) => {
    if(to.meta.auth && !userStore.isLoggedIn){
      next('/users/login');
    } else if(!to.meta.auth && userStore.isLoggedIn && to.name !== '404'){
      next('/');
    } else {
      next();
    }
  });

})
