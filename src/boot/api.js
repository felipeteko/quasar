import {boot} from "quasar/wrappers";
import axios from "axios";
import {getToken, removeToken} from './storage';
import {Loading, Dialog, Notify} from "quasar";

const api = axios.create({ baseURL: process.env.API_URL });

async function loadToken(){
  try {
    const token = await getToken();
    api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } catch (e){
    api.defaults.headers.common['Authorization'] = '';
  }
}

async function post(endpoint, params, loadingMessage = 'Cargando'){
  try {
    Loading.show({
      message: loadingMessage
    });
    const {data} = await api.post(endpoint, params);
    Loading.hide();
    if(data.error){
      Dialog.create({
        class: 'dialog-error',
        title: 'Error',
        message: data.message,
        ok: {
          color: 'negative',
          label: 'Aceptar'
        },
      }).onDismiss(async () => {
        if(data.reauth){
          await removeToken();
          location.reload();
        }
      });
    }
    return data;
  } catch (e) {
    Loading.hide();
    Notify.create({
      color: 'red-5',
      icon: 'warning',
      textColor: 'white',
      message: 'No se pudo conectar con el servidor'
    });
    return {error: true, message: 'No se pudo conectar con el servidor'};
  }
}

export default boot(async () => {
  await loadToken();
});

export { api, loadToken, post };
