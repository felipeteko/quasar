import { defineStore } from 'pinia';
import {getToken, setToken, removeToken} from "boot/storage";
import {loadToken, post} from "boot/api";

export const useUserStore = defineStore('user', {
  state(){
    return {
      user: null
    }
  },
  getters: {
    isLoggedIn(state){
      return state.user !== null;
    }
  },
  actions: {
    async login(params) {
      const data = await post('/users/login', params, 'Validando información');
      if(!data.error){
        await setToken(data.token); //Save Token in Storage
        await loadToken(); //Load Token in API Headers
        this.user = data.user;
      }
    },
    async validate() {
      const token = await getToken();
      if(!token){
        return;
      }
      const data = await post('/users/validate', {token}, 'Validando token');
      if(!data.error){
        this.user = data.user;
      }
    },
    async logout() {
      await removeToken();
      this.user = null;
    }
  }
})
