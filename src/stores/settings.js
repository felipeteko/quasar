import { defineStore } from 'pinia';

export const useSettingsStore = defineStore('settings', {
  state(){
    return {
      title: 'Kidsy'
    }
  },
  actions: {
    setTitle(title){
      this.title = title;
    }
  }
})
